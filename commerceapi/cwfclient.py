from collections import namedtuple
from urllib.parse import quote as url_encode
from datetime import datetime
from uuid import uuid4
from functools import wraps
from .cwfexceptions import CommerceApiException
from .base_api_client import CwfBaseClient, Resources

Configuration = namedtuple('Configuration', 'Url, Origin, ApiKey')

# Internal Use
_address = namedtuple('Response', 'AddressLine1, AddressLine2, City, State, Country, PostalCode')
_EMPTY_ADDR = _address("", "", "", "", "", "")
_REF_DATE = datetime(1, 1, 1)
_LOGGED_IN_HEADER = {"X-Logged-In-Status": "TRUE"}


def check_and_convert_2ticks(dt):
    """
    Equivalent to C# DateTime.Ticks
    """
    if isinstance(dt, datetime):
        # check for time aware datetime and remove.  Still expecting a UTC datetime in either case.
        if dt.tzinfo is not None and dt.tzinfo.utcoffset(dt) is not None:
            dt = dt.replace(tzinfo=None)
        return int((dt - _REF_DATE).total_seconds()) * 10000000
    else:
        return dt


def safe(args):
    email = args.get("email", None)
    if email is not None:
        args["email"] = url_encode(email)
    return args


class ShoppingCartItemType:
    Unspecified = 0
    AdmissionTicket = 1
    OccurrenceTicket = 2
    Donation = 3
    ElectronicGiftCard = 4
    PlasticGiftCard = 5


class CwfClient(CwfBaseClient):
    def __init__(self, config):
        super().__init__(config.Url, {"X-Origin": config.Origin, "Ocp-Apim-Subscription-key": config.ApiKey})


class GiftCardService(Resources):
    """
        GetBalance(data={"Number": "<GC Card Number>"})
    """
    class _resource:
        GetBalance = ("POST", "giftcard/balance")


class AzureB2CService(Resources):
    """
        GetAzureAccount(objectId=<objectId>)
        UpdateAzureAccount(data=<AzureAccount>)
    """
    class _resource:
        GetAzureAccount = ("GET", "AzureB2CAccount/{objectId}")
        UpdateAzureAccount = ("PUT", "AzureB2CAccount")


class FulfilledTicketRepository(Resources):
    """
        ValidateTicket(id=<id>)
        ImportTicketsFromOrder(orderId=<orderId>)
        ImportTicketsFromEmail(email=<email>)
    """
    def _post(self, url, **kwargs):
        kwargs['data'] = {}
        return super()._post(url, **safe(kwargs))

    class _resource:
        ValidateTicket = ("GET", "tickets/fulfilled/{id}/validate")
        ImportTicketsFromOrder = ("POST", "tickets/fulfilled/import/{orderId}")
        ImportTicketsFromEmail = ("POST", "tickets/fulfilled/import/{email}")


class CodeRepository(Resources):
    """
        GetAllCountries()
        GetAllRegions()
        GetRegionsByCountryCode(countryCode=<code>)
    """
    class _resource:
        GetAllCountries = ("GET", "countries")
        GetAllRegions = ("GET", "regions")
        GetRegionsByCountryCode = ("GET", "regions")


class AdmissionTicketRepository(Resources):
    """
        GetAll()
        GetById(id=<id>)
    """
    class _resource:
        GetAll = ("GET", "tickets")
        GetById = ("GET", "tickets/{id}")


class OccurrenceSearchService(Resources):
    """
        Find(startDate=<Datetime>, endDate=<DateTime>, universalLocationId=<id>, eventCode=<eventCode>)
        FindById(id=<id>)
        GetEventCodes(startDate=<Datetime>, endDate=<DateTime>)
        GetSellableDates(eventCode=<eventCode>, startDate=<Datetime>, endDate=<DateTime>, timeZone="Eastern Standard Time")
    """
    def _getp(self, url, **kwargs):
        kwargs['params'] = {k: check_and_convert_2ticks(v) for k, v in kwargs.items()}
        return super()._getp(url, **kwargs)

    class _resource:
        GetEventCodes = ("GETP", "eventcodes")
        Find = ("GETP", "occurrences")
        FindById = ("GET", "occurrences/{id}")
        GetSellableDates = ("GETP", "occurrences/{eventCode}/dates")


class CustomerRepository(Resources):
    """
    Create(data={})
    GetCustomerByEmail(email=<email>)
    GetCustomerById(id=<id>)
    UpdateCustomer(data={})
    GetOrderHistory(email=<email>)
    GetActiveCart(email=<email>)
    GetSavedPayments(email=<email>)
    AddSavedPayment(email=<email>, data={})
    UpdateSavedPaymentBillingAddress(email=<email>, paymentId=<paymentId>, data={})
    DeleteSavedPayment(email=<email>, paymentId=<paymentId>)
    GetActiveTickets(email=<email>)
    CanFulfillToday(email=<email>)
    """
    def __init__(self):
        super().__init__()
        self.GetOrderHistory = self._validate_customer(should_be_present=True)(self.GetOrderHistory)
        self.Create = self._validate_customer(should_be_present=False)(self.Create)
        self.GetSavedPayments = self._validate_customer_for_saved_payments(self.GetSavedPayments)
        self.GetCustomerByEmail = self._check_for_address(self.GetCustomerByEmail)
        self.CanFulfillToday = self._validate_can_fulfill_today()(self.CanFulfillToday)

    def _get(self, url, **kwargs):
        obj = super()._get(url, **safe(kwargs))
        if obj.__class__.__name__ == "CartResponse":
            client_cart = ClientShoppingCart()
            client_cart.ShoppingCart = obj
            return client_cart
        return obj

    def _post(self, url, **kwargs):
        return super()._post(url, **safe(kwargs))

    def _put(self, url, **kwargs):
        if "data" not in kwargs:
            kwargs['data'] = {}
        return super()._put(url, **safe(kwargs))

    @staticmethod
    def _check_for_address(resource):
        def _decorator(*args, **kwargs):
            customer = resource(*args, **kwargs)
            if customer is not None and customer.Address is None:
                return customer._replace(Address=_EMPTY_ADDR)
            return customer
        return wraps(resource)(_decorator)

    # call with parameter and self
    def _validate_customer(self, should_be_present=None):
        def _validate_customer_internal(resource):
            def _decorator(*args, **kwargs):
                # pre call
                email = kwargs.get('email', None)
                if email is None:
                    email = kwargs['data']['EmailAddress']

                customer = self.GetCustomerByEmail(email=email)
                if should_be_present and customer is None:
                    raise CommerceApiException(f"Customer {email} does not exist")

                if not should_be_present and customer is not None:
                    raise CommerceApiException(f"Customer {email} already exist")

                # rest call
                response = resource(*args, **kwargs)

                # post call
                # do something if need be
                return response
            return wraps(resource)(_decorator)
        return _validate_customer_internal

    @staticmethod
    def _validate_customer_for_saved_payments(resource):
        def _decorator(*args, **kwargs):
            response = resource(*args, **kwargs)
            if response is None:
                raise CommerceApiException(f"Customer {kwargs['email']} does not exist")
            return response.SavedPayments
        return wraps(resource)(_decorator)

    def _validate_can_fulfill_today(self):
        def _validate_can_fulfill_today_internal(resource):
            def _decorator(*args, **kwargs):
                email = kwargs.get('email', None)
                if email is None:
                    raise CommerceApiException("Missing E-mail Parameter")
                customer = self.GetCustomerByEmail(email=email)
                if customer is None:
                    raise CommerceApiException(f"Customer {email} does not exist")
                response = resource(*args, **kwargs)
                return response.IsFulfillable
            return wraps(resource)(_decorator)
        return _validate_can_fulfill_today_internal

    class _resource:
        Create = ("POST", "customer")
        GetOrderHistory = ("GET", "customer/{email}/orderhistory")
        GetCustomerByEmail = ("GET", "customer/{email}")
        GetActiveCart = ("GET", "customer/{email}/cart")
        GetCustomerById = ("GET", "customer/{id}")
        UpdateCustomer = ("PUT", "customer")
        UpdateSavedPaymentBillingAddress = ("PUT", "customer/{email}/updatetokenaddress/{paymentId}")
        DeleteSavedPayment = ("PUT", "customer/{email}/deletetoken/{paymentId}")
        GetSavedPayments = ("GET", "customer/{email}")
        AddSavedPayment = ("PUT", "customer/{email}/token")
        GetActiveTickets = ("GET", "customer/{email}/activetickets")
        CanFulfillToday = ("GET", "customer/{email}/canfulfill")


class ShoppingCartRepository(Resources):
    """
        GetById(id="<id>")
        Create()
        Fulfill(id="<id>")
    """
    def _get(self, url, **kwargs):
        client_cart = ClientShoppingCart()
        client_cart.ShoppingCart = super()._get(url, **kwargs)
        return client_cart

    def _put(self, url, **kwargs):
        kwargs['id'] = uuid4()
        kwargs['data'] = {}
        client_cart = ClientShoppingCart()
        client_cart.ShoppingCart = super()._put(url, **kwargs)
        return client_cart

    def _post(self, url, **kwargs):
        if 'data' not in kwargs:
            kwargs['data'] = {}
        return super()._post(url, **kwargs)

    class _resource:
        GetById = ("GET", "carts/{id}")
        Create = ("PUT", "carts/{id}")
        Fulfill = ("POST", "order/{id}/fulfill")


class ClientShoppingCart(Resources):
    """
        AddDonation(data={"Amount": 0.0})
        UpdateDonation(data={"Amount": 0.0})
        DeleteDonation()

        AddGiftCard(data={"ShoppingCartItemType": ShoppingCartItemType.ElectronicGiftCard, "Amount": 0.0, "GiftCardInfo": Visitor})
        ModifyGiftCard(itemId="<id>", data={"Amount": 0.0, "GiftCardInfo": Visitor});
        DeleteGiftCard(itemId="<id>");

        AddGiftCardPayment(data={newPaymentRequest})
        DeleteGiftCardPayment(value="<value>")

        ModifyLineItem(itemId="<id>", data={})
        AddTicket(data={"ProductId": ticketId, "AgeId": ageCategoryId, "Quantity": quantity, "DateOfFirstVisit": dateOfFirstVisit})
        AddOccurrence(data={"ProductId: occurrenceId, "AgeId": ageCategoryId, "Quantity": quantity})
        RemoveItem(itemId="<id>")

        AddPromotionCode(data={"Code": code})
        RemovePromotionCode()

        AddExistingVisitor(email=email)
        AddVisitor(data={visitor})
        UpdateVisitor(data={visitor})

        Complete_NewPayment(data={newPaymentRequest})
        Complete_SavedPayment(data={savedPaymentRequest})
        ImportCart(cartId=<cartId>)
        ApplyPromotions()

        Complete_PayPal(data={pay-palPaymentRequest})
        PayPalPaymentInit()

        ReleaseInventory(itemId="<itemId>")
        LockInventory(itemId="<itemId>")
        ValidateInventoryHolds()
        SetActive()
        SetInactive()

        IsFulfillable()
        Fulfill()
    """

    def __init__(self):
        super().__init__()
        self.AddExistingVisitor = self._process_AddExistingVisitor(self.AddExistingVisitor)
        self.UpdateVisitor = self._process_UpdateVisitor(self.UpdateVisitor)
        self.Complete_NewPayment = self._validate_cart_for_complete(self.Complete_NewPayment)
        self.Complete_SavedPayment = self._validate_cart_for_complete(self.Complete_SavedPayment)
        self.Complete_PayPal = self._validate_cart_for_complete(self.Complete_PayPal)
        self.ValidateInventoryHolds = self._process_InventoryHolds(self.ValidateInventoryHolds)
        self.IsFulfillable = self._process_isFulfillable(self.IsFulfillable)
        self.AddVisitor = self._process_AddVisitor(self.AddVisitor)
        self.UpdateVisitor = self._process_AddVisitor(self.UpdateVisitor)
        # Add header info for logged in users needed for membership promotions
        self.AddTicket = self._add_header(self.AddTicket)
        self.ModifyLineItem = self._add_header(self.ModifyLineItem)
        self.AddOccurrence = self._add_header(self.AddOccurrence)
        self.RemoveItem = self._add_header(self.RemoveItem)
        self.AddPromotionCode = self._add_header(self.AddPromotionCode)
        self.RemovePromotionCode = self._add_header(self.RemovePromotionCode)
        self.ImportCart = self._add_header(self.ImportCart)
        self.AddGiftCard = self._add_header(self.AddGiftCard)
        self.ModifyGiftCard = self._add_header(self.ModifyGiftCard)
        self.DeleteGiftCard = self._add_header(self.DeleteGiftCard)
        self.ApplyPromotions = self._add_header(self.ApplyPromotions)

    def _get(self, url, **kwargs):
        kwargs['id'] = self.ShoppingCart.id
        obj = super()._get(url, **kwargs)
        if obj is not None:
            self.ShoppingCart = obj

    def _post(self, url, **kwargs):
        kwargs['id'] = self.ShoppingCart.id
        if 'data' not in kwargs:
            kwargs['data'] = {}
        obj = super()._post(url, **kwargs)
        if obj is not None:
            self.ShoppingCart = obj

    def _put(self, url, **kwargs):
        kwargs['id'] = self.ShoppingCart.id
        if 'data' not in kwargs:
            kwargs['data'] = {}
        obj = super()._put(url, **kwargs)
        if obj is not None:
            self.ShoppingCart = obj

    def _delete(self, url, **kwargs):
        kwargs['id'] = self.ShoppingCart.id
        obj = super()._delete(url, **kwargs)
        if obj is not None:
            self.ShoppingCart = obj

    # TODO Create backend service to do this instead of doing it at the client level
    def _process_AddExistingVisitor(self, resource):
        def _decorator(*args, **kwargs):
            c = super(ClientShoppingCart, self)._get(self._resource.AddExistingVisitor[1], **safe(kwargs))
            if c is None:
                raise CommerceApiException(f"Customer {kwargs['email']} does not exist")
            address = _EMPTY_ADDR if c.Address is None else c.Address
            visitor = {"AlternateEmailAddress": None, "FirstName": c.FirstName, "LastName": c.LastName, "EmailAddress": c.EmailAddress, "PhoneNumber": c.PhoneNumber, "MobilePhoneNumber": c.MobilePhoneNumber, "Address": address}
            self.AddVisitor(data=visitor)
        return wraps(resource)(_decorator)

    def _process_UpdateVisitor(self, resource):
        def _decorator(*args, **kwargs):
            if self.ShoppingCart.Visitor is None:
                raise CommerceApiException("Can't update Visitor, does not exists!")
            resource(*args, **kwargs)
        return wraps(resource)(_decorator)

    def _validate_cart_for_complete(self, resource):
        def _decorator(*args, **kwargs):
            if len(self.ShoppingCart.Items) == 0:
                raise CommerceApiException("Cannot complete transaction on an empty cart.")
            if any("PAY" in i.Value for i in self.ShoppingCart.Transactions):
                raise CommerceApiException("This shopping cart has already been completed.")
            resource(*args, **kwargs)
        return wraps(resource)(_decorator)

    def _process_InventoryHolds(self, resource):
        def _decorator(*args, **kwargs):
            resource(*args, **kwargs)
            return [i for i in self.ShoppingCart.Items if i.InventoryStatus == 2]
        return wraps(resource)(_decorator)

    def _process_isFulfillable(self, resource):
        def _decorator(*args, **kwargs):
            kwargs['id'] = self.ShoppingCart.id
            obj = super(ClientShoppingCart, self)._get(self._resource.IsFulfillable[1], **kwargs)
            return obj.IsFulfillable
        return wraps(resource)(_decorator)

    def _process_AddVisitor(self, resource):
        def _decorator(*args, **kwargs):
            cart = self.ShoppingCart
            resource(*args, **kwargs)

            d = kwargs['data']
            self.ShoppingCart = cart._replace(Visitor=namedtuple('Response', d.keys())(*d.values()))
        return wraps(resource)(_decorator)

    def _add_header(self, resource):
        def _decorator(*args, **kwargs):
            logged_in = kwargs.get('logged_in', None)
            if logged_in is not None and logged_in["State"]:
                kwargs['extra_headers'] = _LOGGED_IN_HEADER
            resource(*args, **kwargs)
        return wraps(resource)(_decorator)

    class _resource:
        AddGiftCard = ("POST", "carts/{id}/giftcard")
        ModifyGiftCard = ("PUT", "carts/{id}/giftcard/{itemId}")
        DeleteGiftCard = ("DELETE", "carts/{id}/item/{itemId}")

        AddDonation = ("POST", "carts/{id}/donation")
        UpdateDonation = ("PUT", "carts/{id}/donation")
        DeleteDonation = ("DELETE", "carts/{id}/donation")

        ModifyLineItem = ("PUT", "carts/{id}/item/{itemId}")
        AddTicket = ("POST", "carts/{id}/tickets")
        AddOccurrence = ("POST", "carts/{id}/occurrences")
        RemoveItem = ("DELETE", "carts/{id}/item/{itemId}")
        ReleaseInventory = ("PUT", "carts/{id}/releaseitem/{itemId}")
        LockInventory = ("PUT", "carts/{id}/lockitem/{itemId}")

        AddPromotionCode = ("PUT", "carts/{id}/promotioncode")
        RemovePromotionCode = ("DELETE", "carts/{id}/promotion")

        AddExistingVisitor = ("GET", "customer/getbyemail/{email}")
        AddVisitor = ("PUT", "carts/{id}/visitor")
        UpdateVisitor = ("PUT", "carts/{id}/visitor")

        Complete_NewPayment = ("POST", "carts/{id}/payments")
        Complete_SavedPayment = ("POST", "carts/{id}/payments")
        Complete_PayPal = ("POST", "paypal-pay")
        PayPalPaymentInit = ("POST", "carts/{id}/paypal-init")

        ImportCart = ("PUT", "carts/{id}/import/{cartId}")
        ApplyPromotions = ("POST", "carts/{id}/apply-discounts")
        ValidateInventoryHolds = ("POST", "carts/{id}/ValidateInventoryHolds")
        IsFulfillable = ("GET", "carts/{id}/isfulfillabletoday")
        Fulfill = ("POST", "order/{id}/fulfill")

        SetActive = ("PUT", "carts/{id}/seactive")
        SetInactive = ("PUT", "carts/{id}/setinactive")

        AddGiftCardPayment = ("POST", "carts/{id}/PaymentGiftCards")
        DeleteGiftCardPayment = ("DELETE", "carts/{id}/PaymentGiftCards/{value}")
