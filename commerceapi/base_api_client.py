from http import HTTPStatus
from datetime import datetime, timedelta
from collections import namedtuple
from functools import singledispatch
from .cwfexceptions import *
import requests
import simplejson as json


@singledispatch
def to_serializable(val):
    """Used by default."""
    return str(val)


@to_serializable.register(datetime)
def ts_datetime(val):
    """Used if *val* is an instance of datetime."""
    return val.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"


def _json_object_hook(d):
    """Check to see if named tuple is a shopping cart and return CartResponse class when needed"""
    if "Origin" in d:
        return CreateBaseShoppingCartResponse(d)
    return namedtuple('Response', d.keys())(*d.values())


def json2obj(data):
    """Convert json to obj wrapper"""
    return json.loads(data, object_hook=_json_object_hook) if data != "" else None


def obj2json(obj):
    """configure serializer"""
    return json.dumps(obj, default=to_serializable)


def CreateBaseShoppingCartResponse(d):
    """return a read only class based off a named tuple with additional helper properties for the shopping cart"""
    class CartResponse(namedtuple('Response', d.keys())):
        @property
        def TotalPrice(self):
            return round(sum((i.Price * i.Quantity for i in self.Items)), 2)

        @property
        def TotalTaxesAndFees(self):
            return round(sum((i.TaxesAndFees * i.Quantity for i in self.Items)), 2)

        @property
        def TotalOriginalPrice(self):
            return round(sum((i.OriginalPrice * i.Quantity for i in self.Items)), 2)

        @property
        def TotalOriginalTaxesAndFees(self):
            return round(sum((i.OriginalTaxesAndFees * i.Quantity for i in self.Items)), 2)

        @property
        def GrandTotal(self):
            return self.TotalPrice + self.TotalTaxesAndFees

        @property
        def AmountDue(self):
            return self.GrandTotal - round(sum((i.Amount for i in self.Transactions if i.TransactionType == 3)), 2)

    return CartResponse(*d.values())


class CwfBaseClient:
    """client base that contains a list of all the service repositories available in the SDK"""
    registry = {}

    def __init__(self, base_url, headers):
        base_headers = {'Content-type': 'application/json'}
        Resources._baseUrl = base_url
        Resources._headers = {**headers, **base_headers}
        for key, val in self.registry.items():
            setattr(self, key, val())


class ResourceMeta(type):
    """Meta class for Resources initializations and self registration to the base client"""
    def __new__(mcs, name, bases, class_dict):
        cls = type.__new__(mcs, name, bases, class_dict)
        if cls.__name__ != "Resources":
            for key, val in cls._resource.__dict__.items():
                if key.startswith("__"):
                    continue
                setattr(cls, key, mcs._make_def(f"_{val[0].lower()}", val[1]))
            if cls.__name__ != "ClientShoppingCart":
                CwfBaseClient.registry[cls.__name__] = cls
        return cls

    @staticmethod
    def _make_def(verb, url):
        def _def(self, **kw):
            return getattr(self, verb)(url, **kw)
        return _def


class Resources(metaclass=ResourceMeta):
    """Base class used by all service and repository entities needed by the SDK"""

    def _get(self, url, **kwargs):
        return json2obj(self._status_check(requests.get(f"{Resources._baseUrl}{url.format(**kwargs)}", headers=Resources._headers)).text)

    def _getp(self, url, **kwargs):
        return json2obj(self._status_check(requests.get(f"{Resources._baseUrl}{url.format(**kwargs)}", headers=Resources._headers, params=kwargs['params'])).text)

    def _post(self, url, **kwargs):
        extra_headers = kwargs.get('extra_headers', None)
        header = Resources._headers if extra_headers is None else {**extra_headers, **Resources._headers}
        return json2obj(self._status_check(requests.post(f"{Resources._baseUrl}{url.format(**kwargs)}", headers=header, data=obj2json(kwargs['data']))).text)

    def _put(self, url, **kwargs):
        extra_headers = kwargs.get('extra_headers', None)
        header = Resources._headers if extra_headers is None else {**extra_headers, **Resources._headers}
        return json2obj(self._status_check(requests.put(f"{Resources._baseUrl}{url.format(**kwargs)}", headers=header, data=obj2json(kwargs['data']))).text)

    def _delete(self, url, **kwargs):
        extra_headers = kwargs.get('extra_headers', None)
        header = Resources._headers if extra_headers is None else {**extra_headers, **Resources._headers}
        return json2obj(self._status_check(requests.delete(f"{Resources._baseUrl}{url.format(**kwargs)}", headers=header)).text)

    @staticmethod
    def _status_check(res):
        if HTTPStatus.OK <= res.status_code < HTTPStatus.MULTIPLE_CHOICES:
            return res

        if res.status_code == HTTPStatus.UNAUTHORIZED:
            raise CommerceApiAuthenticationException(f'{res.status_code} - {res.content}')

        if res.status_code == HTTPStatus.BAD_REQUEST:

            if str(res.content).startswith("Quantity could not be held for performance."):
                raise InventoryHoldFailedException(f'{res.content}')

            if str(res.content).startswith("Delta quantity could not be held for performance."):
                raise InventoryHoldFailedException(f'{res.content}')

            if str(res.content).startswith("No promotion exists for code."):
                raise PromoCodeDoesNotExistException(f'{res.content}')

            if str(res.content).startswith("Promotion could not be applied to cart."):
                raise PromoNotAppliedException(f'{res.content}')

        if str(res.content).startswith("Shopping Cart amount does not match PayPal amount!"):
            raise ApiPayPalExceptionAmountMismatch(f'{res.content}')

        if "Credit Card Processing Error" in str(res.content):
            raise ApiCreditCardException(f'{res.content}')

        raise CommerceApiException(f'{res.status_code} - {res.content}')
