class CommerceApiAuthenticationException(Exception):
    pass


class InventoryHoldFailedException(Exception):
    pass


class PromoCodeDoesNotExistException(Exception):
    pass


class PromoNotAppliedException(Exception):
    pass


class ApiCreditCardException(Exception):
    pass


class CommerceApiException(Exception):
    pass


class ApiPayPalExceptionAmountMismatch(Exception):
    pass
