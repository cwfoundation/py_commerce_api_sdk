from distutils.core import setup

setup(
    name='CwfCommerceAPI',
    version='0.1.12',
    packages=['commerceapi'],
    description='',
    install_requires=[
        "requests",
        "simplejson",
    ],
)

